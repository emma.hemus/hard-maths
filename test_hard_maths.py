from hard_maths import adding, multiplication

def test_add():
    assert adding(1, 1) == 2, "That just don't add up"

def test_multiplication():
    assert multiplication(7, 5) == 35